//
//  ResponseModel.swift
//  Anonymous
//
//  Created by Sandeep Agrawal on 02/08/16.
//  Copyright © 2016 MotionsApp. All rights reserved.
//

import UIKit

class ResponseModel: NSObject {

    var status:String?
    var count:Int?
    var count_total :Int?
    var pages :Int?
    var error :Bool?
    var posts :[StoryModel] = []
}
