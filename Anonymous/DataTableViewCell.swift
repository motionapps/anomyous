//
//  DataTableViewCell.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/8/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

struct DataTableViewCellData {
    
  
    var imageUrl: String
    var viewCount: String
    var category: String
    var detail: String
    
    init(imageUrl: String, viewCount: String,category: String, detail: String) {
        self.imageUrl = imageUrl
        self.viewCount = viewCount
        self.category = category
        self.detail = detail
    }
    
}

class DataTableViewCell : BaseTableViewCell {
    
    @IBOutlet weak var dataImage: UIImageView!
    @IBOutlet weak var viewCountText: UILabel!
    @IBOutlet weak var categoryText: UILabel!
    @IBOutlet weak var detailText: UILabel!
    
    override func awakeFromNib() {
//        self.viewCountText?.font = UIFont.italicSystemFontOfSize(16)
        self.viewCountText?.textColor = UIColor.whiteColor()
        self.detailText?.textColor = UIColor.whiteColor()
        self.categoryText.textColor = UIColor.whiteColor()
    }
 
    override class func height() -> CGFloat {
        return 200
    }
    
//    override func setData(data: Any?) {
//        if (data as! DataTableViewCellData) != nil {
//            self.dataImage.setRandomDownloadImage(80, height: 80)
//            self.viewCountText.text = data.
//            self.detailText.text =
//        }

//    }
    
//    override func setData(data: Any?) {
//        if(data as! DataTableViewCellData) ! = nil{
//            self.viewCountText.text = data.
//        }
//    }
}
