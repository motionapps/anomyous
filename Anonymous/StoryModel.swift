//
//  StoryModel.swift
//  Anonymous
//
//  Created by Sandeep Agrawal on 02/08/16.
//  Copyright © 2016 MotionsApp. All rights reserved.
//

import UIKit

class StoryModel: NSObject {

    var id:Int?
    var categories :[CategoryModel] = []
    var imageUrl :String?
    var title_plain :String?
    var content :String?
    var custom_fields :[CustomFieldModel] = []
    var comment_count :String?
    var thumbnail :String?
    var medium_large :ImageTypeModel?
    var mvp_medium_thumb :ImageTypeModel?
    var excerpt :String?
    var comments :[CommentModel] = []
}
