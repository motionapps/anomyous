//
//  CategoryModel.swift
//  Anonymous
//
//  Created by Sandeep Agrawal on 02/08/16.
//  Copyright © 2016 MotionsApp. All rights reserved.
//

import UIKit

class CategoryModel: NSObject {

    var id: Int?
    var title :String?
}
