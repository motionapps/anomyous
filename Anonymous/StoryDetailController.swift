//
//  StoryDetailController.swift
//  Anonymous
//
//  Created by Harneet Singh on 04/08/16.
//  Copyright © 2016 MotionsApp. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
import Social
class StoryDetailController: UIViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var moreShareView: UIImageView!
    @IBOutlet weak var mailView: UIImageView!
    @IBOutlet weak var pinterestView: UIImageView!
    @IBOutlet weak var twitterView: UIImageView!
    @IBOutlet weak var facebookView: UIImageView!
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var viewCountText: UILabel!
    @IBOutlet weak var categoryText: UILabel!
    @IBOutlet weak var detailText: UIWebView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var commentTableView: UITableView!
    @IBOutlet weak var scrollHeightConstraints: NSLayoutConstraint!
    var height,maxHeight: CGFloat!

    
    
    
    var TablePost : StoryModel!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false

        self.titleText?.textColor = UIColor.whiteColor()
        self.categoryText?.textColor = UIColor.whiteColor()
        detailText.userInteractionEnabled=false
        self.titleText.text = self.TablePost.title_plain
        self.detailText?.loadHTMLString(self.TablePost.excerpt!, baseURL: nil)
        print(self.TablePost.thumbnail)
        self.categoryText.text=self.TablePost.categories[0].title
        self.imageView.image = nil
        ImageLoader.sharedLoader.imageForUrl(self.TablePost.thumbnail!, completionHandler:{(image: UIImage?, url: String) in
            self.imageView.image = image
        })
        
        self.commentTableView.registerCellNib(CommentCellView.self)
        
        self.commentTableView.delegate = self
        self.commentTableView.dataSource = self
        let facebookTapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(StoryDetailController.facebookShare(_:)))
        facebookView.userInteractionEnabled = true
        facebookView.addGestureRecognizer(facebookTapGestureRecognizer)
        
        let twitterTapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(StoryDetailController.twitterShare(_:)))
        twitterView.userInteractionEnabled = true
        twitterView.addGestureRecognizer(twitterTapGestureRecognizer)
        
        let pinterestTapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(StoryDetailController.pinterestShare(_:)))
        pinterestView.userInteractionEnabled = true
        pinterestView.addGestureRecognizer(pinterestTapGestureRecognizer)
        
        let mailTapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(StoryDetailController.mailShare(_:)))
        mailView.userInteractionEnabled = true
        mailView.addGestureRecognizer(mailTapGestureRecognizer)
        
        let moreTapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(StoryDetailController.moreShare(_:)))
        moreShareView.userInteractionEnabled = true
        moreShareView.addGestureRecognizer(moreTapGestureRecognizer)
        
    }
//    override func viewDidAppear(animated: Bool) {
//
//    }
    
    func facebookShare(sender: UITapGestureRecognizer? = nil) {

        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            
            let tweetShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            
            self.presentViewController(tweetShare, animated: true, completion: nil)
            
        } else {
            
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to tweet.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    func twitterShare(sender: UITapGestureRecognizer? = nil) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
            
            let tweetShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            
            self.presentViewController(tweetShare, animated: true, completion: nil)
            
        } else {
            
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to tweet.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func pinterestShare(sender: UITapGestureRecognizer? = nil) {
        
    }
    
    func mailShare(sender: UITapGestureRecognizer? = nil) {
        if MFMailComposeViewController.canSendMail() {
            let emailTitle = "Un mail envoyé depuis un mobile écoutant DuranceFM"
            let toRecipents = ["appli@durancefm.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setToRecipients(toRecipents)
            self.presentViewController(mc, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Alert", message: "No app found to send mail.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func moreShare(sender: UITapGestureRecognizer? = nil) {
        
        let shareContent = "This is dummy text for sharing"
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        presentViewController(activityViewController, animated: true, completion: {})
    }
    
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError?) {
        switch result.rawValue {
        case MFMailComposeResultCancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResultSaved.rawValue:
            print("Mail saved")
        case MFMailComposeResultSent.rawValue:
            print("Mail sent")
        case MFMailComposeResultFailed.rawValue:
            print("Mail sent failure: \(error!.localizedDescription)")
        default:
            break
        }
        controller.dismissViewControllerAnimated(false, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        adjustHeight()
    }
    
    func adjustHeight()
    {
        print(self.commentTableView.superview?.frame.size.height)
        print(self.commentTableView.frame.origin.y)
        print(self.commentTableView.contentSize.height)
        self.height = self.commentTableView.contentSize.height
        self.maxHeight = (self.commentTableView.superview!.frame.size.height) - self.commentTableView.frame.origin.y
        print(maxHeight)
        if (height > maxHeight){
            height = height+self.commentTableView.frame.origin.y;
            print(self.height)
            scrollHeightConstraints.constant=height
        }
        //        else{
        //            height = self.commentTableView.frame.origin.y+10;
        //            print(self.height)
        //            scrollHeightConstraints.constant=height
        //        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CommentCellView.height()
            }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TablePost.comments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.commentTableView.dequeueReusableCellWithIdentifier(CommentCellView.identifier) as! CommentCellView

        let row = indexPath.row
        cell.nameLabel.text=self.TablePost.comments[row].name
        cell.commentWebView.loadHTMLString(self.TablePost.comments[row].content!,baseURL: nil)
        cell.commentWebView.userInteractionEnabled=false
        return cell
    }


}