
//
//  NetworkCall.swift
//
//  Created by Sandeep Agrawal on 25/04/16.
//  Copyright © 2016 motionappsworld. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class NetworkCall {
    var manager: Manager?
    
    init() {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        manager = Alamofire.Manager(configuration: configuration)
        handleCertificate()
        
    }
    
    var networkIP="http://www.motionappsworld.com";//"http://www.anonews.co";//"http://54.191.10.3";
    
    
    func handleCertificate() {
        manager!.delegate.sessionDidReceiveChallenge = { session, challenge in
            var disposition: NSURLSessionAuthChallengeDisposition = .PerformDefaultHandling
            var credential: NSURLCredential?
            
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                disposition = NSURLSessionAuthChallengeDisposition.UseCredential
                credential = NSURLCredential(forTrust: challenge.protectionSpace.serverTrust!)
            } else {
                if challenge.previousFailureCount > 0 {
                    disposition = .CancelAuthenticationChallenge
                } else {
                    credential = self.manager!.session.configuration.URLCredentialStorage?.defaultCredentialForProtectionSpace(challenge.protectionSpace)
                    
                    if credential != nil {
                        disposition = .UseCredential
                    }
                }
            }
            
            return (disposition, credential)
        }
    }
    
    func getPosts(page : Int, count : Int,completionHandler: (responseObject: ResponseModel) -> ()){
        let header = ["Content-Type": "application/json"]
        let responseModel = ResponseModel()
        var posts = [StoryModel]()
        manager!.request(.GET, self.networkIP+"?json=get_recent_posts", parameters: ["page": page,"count" :count],headers: header)
            .responseJSON {  response in
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print(json)
                        responseModel.status = json["status"].stringValue
                        responseModel.count = json["count"].intValue
                        responseModel.count_total = json["count_total"].intValue
                        responseModel.pages = json["pages"].intValue
                        responseModel.error = false
                        for item in json["posts"].arrayValue {
                            var comments = [CommentModel]();
                            var customFields = [CustomFieldModel]()
                            let medium = ImageTypeModel()
                            let high = ImageTypeModel()
                            var postCount = [String]();
                            var categories = [CategoryModel]()
                            let storymodel = StoryModel();
                            storymodel.comment_count = item["comment_count"].stringValue
                            storymodel.content = item["content"].stringValue
                            storymodel.excerpt = item["excerpt"].stringValue
                            storymodel.id = item["id"].intValue
                            storymodel.imageUrl = item["url"].stringValue
                            storymodel.title_plain = item["title_plain"].stringValue
                            storymodel.thumbnail = item["thumbnail"].stringValue
                            medium.url = item["thumbnail_images"]["medium_large"]["url"].stringValue
                            high.url = item["thumbnail_images"]["mvp-medium-thumb"]["url"].stringValue
                            storymodel.medium_large = medium
                            storymodel.mvp_medium_thumb = high
                            
                            for category in item["categories"].arrayValue {
                                let categoryModel = CategoryModel()
                                categoryModel.id = category["id"].intValue
                                categoryModel.title = category["title"].stringValue
                                categories.append(categoryModel)
                            }
                            storymodel.categories = categories
                            
                            for commentobject in item["comments"].arrayValue {
                                let comment = CommentModel()
                                comment.content = commentobject["content"].stringValue
                                comment.name = commentobject["name"].stringValue
                                comments.append(comment)
                            }
                            storymodel.comments = comments
                            
                            for customFieldObject in item["custom_fields"]["post_views_count"].arrayValue {
                                let customField = CustomFieldModel()
                                let count = customFieldObject.stringValue
                                print("count\(count)")
                                postCount.append(count)
                                //                                for post_count in customFieldObject["post_views_count"].arrayValue {
                                //                                    let count = post_count.stringValue
                                //                                    print("count\(count)")
                                //                                    postCount.append(count)
                                //                                }
                                customField.post_views_count = postCount
                                customFields.append(customField)
                            }
                            storymodel.custom_fields = customFields
                            posts.append(storymodel)
                        }
                        responseModel.posts = posts
                        completionHandler(responseObject: responseModel)
                    }
                case .Failure(let error):
                    print(response.result)
                    print(error.localizedFailureReason!)
                    responseModel.error = true
                    completionHandler(responseObject: responseModel)
                }
                
                
        }
    }
    
    func getVideos(playlistId : String, maxResults : String,key : String,completionHandler: (responseObject: [YoutubeVideoModel]) -> ()){
        let header = ["Content-Type": "application/json"]
        var youtubeVideo = [YoutubeVideoModel]();
        manager!.request(.GET, "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet", parameters: ["playlistId": playlistId,"maxResults" :maxResults,"key" : key],headers: header)
            .responseJSON {  response in
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print(json)
                        for item in json["items"].arrayValue {
                            let youtubemodel = YoutubeVideoModel()
                            youtubemodel.channelTitle = item["snippet"]["channelTitle"].stringValue
                            youtubemodel.highThumbnail = item["snippet"]["thumbnails"]["high"]["url"].stringValue
                            youtubemodel.mediumThumbmnail = item["snippet"]["thumbnails"]["medium"]["url"].stringValue
                            youtubemodel.publishedDate = item["snippet"]["publishedAt"].stringValue
                            youtubemodel.title = item["snippet"]["title"].stringValue
                            youtubemodel.videoDescription = item["snippet"]["description"].stringValue
                            youtubemodel.videoId = item["snippet"]["resourceId"]["videoId"].stringValue
                            youtubeVideo.append(youtubemodel)
                        }
                        completionHandler(responseObject: youtubeVideo)
                    }
                case .Failure(let error):
                    print(error)
                }
                
                
        }
    }
    
    func registerDevice(token : String, os : String,completionHandler: (responseObject: Void) -> ()){
        let header = ["Content-Type": "application/x-www-form-urlencoded"]
        manager!.request(.POST, self.networkIP+"/pnfw/register/", parameters: ["token": token,"os" :os],headers: header)
            .responseJSON {  response in
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print(json)
                    }
                case .Failure(let error):
                    print(error)
                }
                
                
        }
    }
    
}

