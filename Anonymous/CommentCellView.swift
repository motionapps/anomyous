//
//  CommentCellView.swift
//  Anonymous
//
//  Created by Harneet Singh on 08/08/16.
//  Copyright © 2016 MotionsApp. All rights reserved.
//

import UIKit

struct CommentCellViewData {
    
    
    var imageUrl: String
    var name: String
    var comment: String
    
    init(imageUrl: String,name: String, comment: String) {
        self.imageUrl = imageUrl
        self.name = name
        self.comment = comment
    }
    
}

class CommentCellView : BaseTableViewCell {
    
    @IBOutlet weak var dataImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentWebView: UIWebView!
    
    override func awakeFromNib() {
        //        self.viewCountText?.font = UIFont.italicSystemFontOfSize(16)
//        self.detailText?.textColor = UIColor.whiteColor()
//        self.categoryText.textColor = UIColor.whiteColor()
    }
    
    override class func height() -> CGFloat {
        return 150
    }
    
    //    override func setData(data: Any?) {
    //        if (data as! DataTableViewCellData) != nil {
    //            self.dataImage.setRandomDownloadImage(80, height: 80)
    //            self.viewCountText.text = data.
    //            self.detailText.text =
    //        }
    
    //    }
    
    //    override func setData(data: Any?) {
    //        if(data as! DataTableViewCellData) ! = nil{
    //            self.viewCountText.text = data.
    //        }
    //    }
}
