//
//  ViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit

class MainViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var indicator : UIActivityIndicatorView!
    var TableData : ResponseModel!
    var TablePost :[StoryModel] = []
    var singleTablePost : StoryModel!
    var TableVideo :[YoutubeVideoModel] = []
    var singleTableVideo :YoutubeVideoModel!
    var page :Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getStories(page, count: 10);
        self.getVideos()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.registerCellNib(DataTableViewCell.self)
        self.tableView.registerCellNib(YoutubeVideoCell.self)

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    @IBAction func SeagmentControlChanged(sender: UISegmentedControl) {
        
        self.do_table_refresh()
        print("SeagmentControlChanged")

        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            // Writec code to bind new data with tableview
           print("First selected")
        case 1:
            print("Second selected")
        default:
            break; 
        }
    }
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getVideos()
    {
        
        let network = NetworkCall()
        network.getVideos("UUJrGShLOmz-phU2reIul_GA", maxResults: "20", key: "AIzaSyCF7oEfc6f1ZeVOskHpf62tRRh_u3WXzOc") { (responseObject) in
        
            for (_,item) in responseObject.enumerate() {
                print(item)
                self.TableVideo.append(item as YoutubeVideoModel)
                
            }
            dispatch_async(dispatch_get_main_queue(), {() -> Void in
                self.do_table_refresh()
            })
            
            
        }
        }
    
    
    func getStories(page : Int, count : Int)
    {
        
        let network = NetworkCall()
        if(page == 1) {
        self.showLoading()
        }
        network.getPosts(page, count: count) { (responseObject:ResponseModel) in
//            print(responseObject)
            
            if(responseObject.error == true) {
                self.hideLoading()
                self.showAlerError("Error", messagetext: "Server Error. Please try again")
                
            } else {
            for (_,item) in responseObject.posts.enumerate() {
                
                self.TablePost.append(item as StoryModel)
                
            }
            
            dispatch_async(dispatch_get_main_queue(), {() -> Void in
                self.do_table_refresh()
                if(page == 1) {
                self.hideLoading()
                }
            })
            }

        }

    }
    
        func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            switch segmentedControl.selectedSegmentIndex
            {
            case 0:
                return DataTableViewCell.height()
            case 1:
                return YoutubeVideoCell.height()
            default:
                return DataTableViewCell.height()
            }
            
        }
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//            print(self.TablePost.count)
            switch segmentedControl.selectedSegmentIndex
            {
            case 0:
                return self.TablePost.count
            case 1:
                return self.TableVideo.count
            default:
                return self.TablePost.count
            }
            
        }
    
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            
            if(segmentedControl.selectedSegmentIndex == 0) {
                let cell = self.tableView.dequeueReusableCellWithIdentifier(DataTableViewCell.identifier) as! DataTableViewCell
                self.singleTablePost = self.TablePost[indexPath.row]
                //            let data = DataTableViewCellData(imageUrl: "dummy", viewCount: self.singleTablePost.comment_count!, category: self.singleTablePost.categories[0].title! ,detail: self.singleTablePost.title_plain!  )
                //            cell.setData(data)
                let row = indexPath.row
                cell.viewCountText.text=self.singleTablePost.custom_fields[0].post_views_count[0]
                cell.categoryText.text=self.singleTablePost.categories[0].title
                print("categoryText "+self.singleTablePost.categories[0].title!)
                print(self.singleTablePost.thumbnail)
                cell.dataImage.image = nil
                ImageLoader.sharedLoader.imageForUrl(self.singleTablePost.thumbnail!, completionHandler:{(image: UIImage?, url: String) in
                    cell.dataImage.image = image
                })
                cell.detailText.text = self.singleTablePost.title_plain!
                if(row == TablePost.count-2) {
                    page += 1
                    self.getStories(page, count: 10);
                }
                return cell
            } else if(segmentedControl.selectedSegmentIndex == 1){
                // Change here for video cell and code
                let cell = self.tableView.dequeueReusableCellWithIdentifier(YoutubeVideoCell.identifier) as! YoutubeVideoCell
                self.singleTableVideo = self.TableVideo[indexPath.row]
                //                let row = indexPath.row
//                print(self.singleTableVideo.videoId!)
                //                cell.dataImage.image = nil
                //                ImageLoader.sharedLoader.imageForUrl(self.singleTableVideo.mediumThumbmnail!, completionHandler:{(image: UIImage?, url: String) in
                //                    cell.dataImage.image = image
                //                })
                //                let myURL = NSURL(string: "https://www.youtube.com/embed/\(self.singleTableVideo.videoId!)")
                //                //Note: use the "embed" address instead of the "watch" address.
                //                let myURLRequest : NSURLRequest = NSURLRequest(URL: myURL!)
                //                cell.player.loadRequest(myURLRequest)
                cell.youtubeplayer.loadVideoID(self.singleTableVideo.videoId!)
                return cell
            }
            else{
            let cell = self.tableView.dequeueReusableCellWithIdentifier(DataTableViewCell.identifier) as! DataTableViewCell

            return cell
            }
        }
    
        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            if(segmentedControl.selectedSegmentIndex == 0){
            
            let storyboard = UIStoryboard(name:"StoryDetailScreen", bundle: nil)
            let viewController = storyboard.instantiateViewControllerWithIdentifier("StoryDetailController") as! StoryDetailController
            viewController.TablePost = self.TablePost[indexPath.row]
            self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    
    
    func showLoading() {
        indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(0.0, 0.0, 80.0, 80.0);
        indicator.center = view.center
        view.addSubview(indicator)
        indicator.bringSubviewToFront(view)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        indicator.startAnimating()
    }
    
    func hideLoading() {
        indicator.stopAnimating()
        indicator.hidden = true
        
    }
    func do_table_refresh()
    {
        self.tableView.reloadData()
        self.tableView.layoutIfNeeded()
    }
    
    func showAlerError(titleText:String,messagetext:String) {
        
        let alertController = UIAlertController(title: titleText, message:
            messagetext, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }


}


extension MainViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
