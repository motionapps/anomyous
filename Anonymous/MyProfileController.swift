//
//  MyProfileController.swift
//  Anonymous
//
//  Created by Harneet Singh on 05/08/16.
//  Copyright © 2016 MotionsApp. All rights reserved.
//

import UIKit



class MyProfileController : UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var username: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2;
        self.profilePic.clipsToBounds = true;
        let image = loadImageFromPath(fileInDocumentsDirectory("profile.jpg"))
        if( image != nil) {
            profilePic.image = loadImageFromPath(fileInDocumentsDirectory("profile.jpg"))
            
        } else {
            profilePic.image = UIImage(named: "user_default")
        
        }
        
        let name = NSUserDefaults.standardUserDefaults().valueForKey("username")
        if(name == nil) {
            username.text = "Guest User"
        } else {
             username.text = name as? String
        }
       
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        
    }
    
    func loadImageFromPath(path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: \(path)")
        }
        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
        
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(filename: String) -> String {
        
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        return fileURL.path!
        
    }
}
