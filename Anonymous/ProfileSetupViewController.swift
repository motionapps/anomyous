//
//  ProfileSetupViewController.swift
//  Anonymous
//
//  Created by Harneet Singh on 27/07/16.
//  Copyright © 2016 MotionsApp. All rights reserved.
//

import UIKit



class ProfileSetupViewController : UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var saveButton: UIButton!
    
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
         imagePicker.delegate = self
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2;
        self.profilePic.clipsToBounds = true;
        let image = loadImageFromPath(fileInDocumentsDirectory("profile.jpg"))
        if( image != nil) {
        profilePic.image = loadImageFromPath(fileInDocumentsDirectory("profile.jpg"))
        
    } else {
    profilePic.image = UIImage(named: "user_default")
    }
        let name = NSUserDefaults.standardUserDefaults().valueForKey("username")
        if(name == nil) {
            username.text = "Guest User"
        } else {
            username.text = name as? String
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(ProfileSetupViewController.pickUpPhoto(_:)))
        profilePic.userInteractionEnabled = true
        profilePic.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    @IBAction func saveButtonClick(sender: UIButton) {
        let image = profilePic.image
        let path = fileInDocumentsDirectory("profile.jpg");
        //Add code to save the username as well
        if((image) != nil) {
            saveImage(image!, path: path)
        }
    }
    func saveImage (image: UIImage, path: String ) -> Bool{
        
        let pngImageData = UIImagePNGRepresentation(image)
        //let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
        let result = pngImageData!.writeToFile(path, atomically: true)
        print("username\(username.text)")
        NSUserDefaults.standardUserDefaults().setObject(username.text!, forKey: "username")
        NSUserDefaults.standardUserDefaults().synchronize()
        return result
        
    }
    
    func pickUpPhoto(sender: UITapGestureRecognizer? = nil) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController(imagePicker, animated: true, completion: nil)
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum) {
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum;
//            imagePicker.allowsEditing = false
//            self.presentViewController(imagePicker, animated: true, completion: nil)
//        } else{
//            print("No source")
//        }
    }
    
    //Callback after selecting pic from photolibrary
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
//        
//        if ( image != nil) {
//        profilePic.image = image
//            
//        } else {
//            print("nil image")
//        }
//        self.dismissViewControllerAnimated(true, completion: nil);
//    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profilePic.contentMode = .ScaleAspectFit
            profilePic.image = pickedImage
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func loadImageFromPath(path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: \(path)")
        }
        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
        
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(filename: String) -> String {
        
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        return fileURL.path!
        
    }

}
