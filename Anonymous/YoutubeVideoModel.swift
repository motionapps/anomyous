//
//  YoutubeVideoModel.swift
//  Anonymous
//
//  Created by Sandeep Agrawal on 02/08/16.
//  Copyright © 2016 MotionsApp. All rights reserved.
//

import UIKit

class YoutubeVideoModel: NSObject {

    var mediumThumbmnail: String?
    var highThumbnail: String?
    var channelTitle:String?
    var publishedDate :String?
    var title:String?
    var videoDescription :String?
    var videoId :String?
}
