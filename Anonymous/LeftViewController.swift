//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit

enum LeftMenu: Int {
    case Main = 0
    case News
    case Politics
    case Resistance
    case Media
    case About
    case profile
    case webview
}

protocol LeftMenuProtocol : class {
    func changeViewController(menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var menus = ["Home", "News", "Politics", "Resistance", "Media","About Anonymous","My Profile","WebView"]
    var mainViewController: UIViewController!
    var swiftViewController: UIViewController!
    var javaViewController: UIViewController!
    var aboutAnonymousController: UIViewController!
    var nonMenuViewController: UIViewController!
    var myProfileViewController: UIViewController!
    var myProfileSetupViewController: UIViewController!
    var webViewViewSetupController: UIViewController!

    @IBOutlet weak var imageHeaderView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        self.tableView.backgroundColor = UIColor(red :255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let swiftViewController = storyboard.instantiateViewControllerWithIdentifier("SwiftViewController") as! SwiftViewController
        self.swiftViewController = UINavigationController(rootViewController: swiftViewController)
        
        let javaViewController = storyboard.instantiateViewControllerWithIdentifier("JavaViewController") as! JavaViewController
        self.javaViewController = UINavigationController(rootViewController: javaViewController)
        
        let aboutAnonymousController = storyboard.instantiateViewControllerWithIdentifier("AboutAnonymous") as! AboutAnonymous
        self.aboutAnonymousController = UINavigationController(rootViewController: aboutAnonymousController)

        let nonMenuController = storyboard.instantiateViewControllerWithIdentifier("NonMenuController") as! NonMenuController
        nonMenuController.delegate = self
        self.nonMenuViewController = UINavigationController(rootViewController: nonMenuController)
        
        let myProfileStoryBoard = UIStoryboard(name: "MyProfile", bundle: nil)
        
        let myProfileController = myProfileStoryBoard.instantiateViewControllerWithIdentifier("MyProfileController") as! MyProfileController
        self.myProfileViewController = UINavigationController(rootViewController: myProfileController)
        
        let myProfileSetupStoryBoard = UIStoryboard(name: "ProfileSetup", bundle: nil)
        
        let myProfileSetupController = myProfileSetupStoryBoard.instantiateViewControllerWithIdentifier("ProfileSetupViewController") as! ProfileSetupViewController
        self.myProfileSetupViewController = UINavigationController(rootViewController: myProfileSetupController)
        
        let webViewSetupStoryBoard = UIStoryboard(name: "WebView", bundle: nil)

        let webViewViewController = webViewSetupStoryBoard.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
        self.webViewViewSetupController = UINavigationController(rootViewController: webViewViewController)
        
//        let storyboard = UIStoryboard(name:"StoryDetailScreen", bundle: nil)
//        let viewController = storyboard.instantiateViewControllerWithIdentifier("StoryDetailController") as! StoryDetailController
        
        self.tableView.registerCellClass(BaseTableViewCell.self)
        
        self.imageHeaderView.layer.cornerRadius = self.imageHeaderView.frame.size.width / 2;
        self.imageHeaderView.clipsToBounds = true;
        self.view.addSubview(self.imageHeaderView)
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(LeftViewController.headerClicked(_:)))
        let tapLabel = UITapGestureRecognizer(target: self, action: #selector(LeftViewController.headerClicked(_:)))

        tapImage.delegate=self
        tapLabel.delegate=self
        
        //sets the user interaction to true, so we can actually track when the image has been tapped
        self.imageHeaderView.userInteractionEnabled = true
        
        //this is where we add the target, since our method to track the taps is in this class
        //we can just type "self", and then put our method name in quotes for the action parameter
        
        //finally, this is where we add the gesture recognizer, so it actually functions correctly
        self.imageHeaderView.addGestureRecognizer(tapImage)
        self.userNameLabel.addGestureRecognizer(tapLabel)
        
    }
    func headerClicked(sender: UITapGestureRecognizer? = nil)
    {
        print("headerClicked")
        
        self.slideMenuController()?.changeMainViewController(self.myProfileSetupViewController, close: true)
        
//        let storyboard = UIStoryboard(name:"ProfileSetup", bundle: nil)
//        let viewController = storyboard.instantiateViewControllerWithIdentifier("ProfileSetupViewController") as! ProfileSetupViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let image = loadImageFromPath(fileInDocumentsDirectory("profile.jpg"))
        if( image != nil) {
            imageHeaderView.image = loadImageFromPath(fileInDocumentsDirectory("profile.jpg"))
            
        } else {
            imageHeaderView.image = UIImage(named: "user_default")
        }
        let name = NSUserDefaults.standardUserDefaults().valueForKey("username")
        if(name == nil) {
            userNameLabel.text = "Guest User"
        } else {
            userNameLabel.text = name as? String
        }

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 160)
        self.view.layoutIfNeeded()
    }
    
    func loadImageFromPath(path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: \(path)")
        }
        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
        
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(filename: String) -> String {
        
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        return fileURL.path!
        
    }
    
    func changeViewController(menu: LeftMenu) {
        switch menu {
        case .Main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .News:
            self.slideMenuController()?.changeMainViewController(self.swiftViewController, close: true)
        case .Politics:
            self.slideMenuController()?.changeMainViewController(self.javaViewController, close: true)
        case .Resistance:
            self.slideMenuController()?.changeMainViewController(self.nonMenuViewController, close: true)
        case .Media:
            self.slideMenuController()?.changeMainViewController(self.nonMenuViewController, close: true)
        case .About:
            self.slideMenuController()?.changeMainViewController(self.aboutAnonymousController, close: true)
        case .profile:
            self.slideMenuController()?.changeMainViewController(self.myProfileViewController, close: true)
        case .webview:
            self.slideMenuController()?.changeMainViewController(self.webViewViewSetupController, close: true)
        }
        
    }
}




//@IBAction func headerClicked(sender: UIImageView) {
//    
//    self.do_table_refresh()
//    print("SeagmentControlChanged")
//    
//}

extension LeftViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.item) {
            switch menu {
            case .Main, .News, .Politics, .Resistance, .Media, .About,.profile,.webview:
                return BaseTableViewCell.height()
            }
        }
        return 0
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.item) {
            switch menu {
            case .Main, .News, .Politics, .Resistance, .Media, .About,.profile,.webview:
                let cell = BaseTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: BaseTableViewCell.identifier)
                cell.setData(menus[indexPath.row])
                cell.backgroundColor = UIColor(red :255/255, green: 255/255, blue: 255/255, alpha: 1.0)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.item) {
            self.changeViewController(menu)
        }
    }
}

extension LeftViewController: UIScrollViewDelegate {
    
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}
