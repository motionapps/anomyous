//
//  WebViewController.swift
//  Anonymous
//
//  Created by Harneet Singh on 12/08/16.
//  Copyright © 2016 MotionsApp. All rights reserved.
//

import Foundation
import UIKit

class WebViewController:UIViewController,UIWebViewDelegate{
    
    @IBOutlet weak var webView: UIWebView!
    var networkIP="http://www.motionappsworld.com";//"http://www.anonews.co";//"http://54.191.10.3";

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false;
//        webView.stringByEvaluatingJavaScriptFromString(<#T##script: String##String#>)
        webView.loadRequest(NSURLRequest(URL: NSURL(string:self.networkIP)!))
        webView.delegate = self;
        NSUserDefaults.standardUserDefaults().registerDefaults(["UserAgent": "MobileWebView"])

    }
    
    
    @IBAction func doRefresh(_: AnyObject) {
        webView.reload()
    }
    
    @IBAction func goBack(_: AnyObject) {
        webView.goBack()
    }
    
    @IBAction func goForward(_: AnyObject) {
        webView.goForward()
    }
    
    @IBAction func stop(_: AnyObject) {
        webView.stopLoading()
    }

    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        print("Webview fail with error \(error)");
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(request.URL?.absoluteString)
        if((request.URL!.absoluteString.hasPrefix("mailto"))) {
            if UIApplication.sharedApplication().canOpenURL(request.URL!) {
                UIApplication.sharedApplication().openURL(request.URL!)
            } else {
                let errorAlert = UIAlertView(title: "Cannot Send Message", message: "Your device is not able to send mail.", delegate: self, cancelButtonTitle: "OK")
                errorAlert.show()
            }
            print("mailto Loading")
            return false
        } else if ((request.URL!.absoluteString.hasPrefix("whatsapp"))) {
            let text = request.URL!.absoluteString.substring(21)
            print("whatsapp\(text)")
            
            if UIApplication.sharedApplication().canOpenURL(request.URL!) {
                UIApplication.sharedApplication().openURL(request.URL!)
            } else {
                let errorAlert = UIAlertView(title: "Cannot Send Message", message: "Your device is not able to send WhatsApp messages.", delegate: self, cancelButtonTitle: "OK")
                errorAlert.show()
            }
            print("whatsapp loading")
            return false
        }
        print("Webview overide Loading")
        return true;
    }
    func webViewDidStartLoad(webView: UIWebView) {
        print("Webview started Loading")
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        print("Webview did finish load")
    }
    
}
